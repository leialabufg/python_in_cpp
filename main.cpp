#include "vsPyInterpreter.hpp"
#include <string>

int main(){
    VsPyInterpreter vpi;
    std::string script_name = "script";
    std::string method_name = "do_strategy";
    vpi.config(script_name, method_name);
    vpi.run();
}