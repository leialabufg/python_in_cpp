from vsClasses import Robot, Ball

def do_strategy(allies, enemies, ball):
	print("allies[0].pos_x = {}".format(allies[0].pos_x))
	print("allies[1].pos_x = {}".format(allies[1].pos_x))
	print("allies[2].pos_x = {}".format(allies[2].pos_x))
	print("enemies[0].pos_x = {}".format(enemies[0].pos_x))
	print("enemies[1].pos_x = {}".format(enemies[1].pos_x))
	print("enemies[2].pos_x = {}".format(enemies[2].pos_x))
	print("ball.pos_x = {}".format(ball.pos_x))
	return (allies, enemies, ball)
