#include "vsPyInterpreter.hpp"

VsPyInterpreter::VsPyInterpreter(){
	Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\".\")");
}

VsPyInterpreter::~VsPyInterpreter(){
	Py_DECREF(pAlliedRobotsTuple);
    Py_DECREF(pEnemyRobotsTuple);
    Py_DECREF(pArgs);
    Py_XDECREF(pModule);
    Py_XDECREF(pFunc);

	Py_Finalize();
}


void VsPyInterpreter::setFloatAttribute(PyObject* pyInstance, const char* attr_name, float val){

    PyObject* pyVal = PyFloat_FromDouble(val);
    PyObject_SetAttrString(pyInstance, attr_name, pyVal);
    Py_DECREF(pyVal);

}

double VsPyInterpreter::getFloatAttribute(PyObject* pyInstance, const char* attr_name){
    PyObject* pyVal = PyObject_GetAttrString(pyInstance, attr_name);
    double val = 0;
    if(pyVal != NULL){
        val = PyFloat_AsDouble(pyVal);
        Py_DECREF(pyVal);
    }
    return val;
}


void VsPyInterpreter::setLongAttribute(PyObject* pyInstance, const char* attr_name, long val){

    PyObject* pyVal = PyLong_FromLong(val);
    PyObject_SetAttrString(pyInstance, attr_name, pyVal);
    Py_DECREF(pyVal);

}

long VsPyInterpreter::getLongAttribute(PyObject* pyInstance, const char* attr_name){
    PyObject* pyVal = PyObject_GetAttrString(pyInstance, attr_name);
    long val = 0;
    if(pyVal != NULL){
        val = PyLong_AsLong(pyVal);
        Py_DECREF(pyVal);;
    }
    return val;
}

int VsPyInterpreter::config(std::string script_name, std::string method_name){

	const char* name = script_name.c_str();
    const char* method = method_name.c_str();

    PyObject *pName, *pDict;

    pName = PyUnicode_DecodeFSDefault(name);
    /* Error checking of pName left out */
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule != NULL) {
        // pDict is a borrowed reference 
        pDict = PyModule_GetDict(pModule);

        this->pFunc = PyObject_GetAttrString(pModule, method);
        /* pFunc is a new reference */

        if (this->pFunc && PyCallable_Check(this->pFunc)) {
            this->pAlliedRobots[3];
            this->pEnemyRobots[3];
            this->pBall;
            PyObject *pClass;

            pClass = PyDict_GetItemString(pDict, "Robot");
            for(int i = 0; i < 3; i++){    
                if(!PyCallable_Check(pClass)){
                    fprintf(stderr, "Cannot find class \"Robot\"\n");
                    Py_DECREF(pFunc);
                    Py_DECREF(pModule);
                    return 0;
                }
                this->pAlliedRobots[i] = PyObject_CallObject(pClass, NULL); 
                
            }

            for(int i = 0; i < 3; i++){
                if(!PyCallable_Check(pClass)){
                    fprintf(stderr, "Cannot find class \"Robot\"\n");
                    Py_DECREF(pFunc);
                    Py_DECREF(pModule);
                    return 0;
                }
                this->pEnemyRobots[i] = PyObject_CallObject(pClass, NULL); 
            }

            pClass = PyDict_GetItemString(pDict, "Ball");
            if(!PyCallable_Check(pClass)){
                fprintf(stderr, "Cannot find class \"Ball\"\n");
                Py_XDECREF(pFunc);
                Py_DECREF(pModule);
                return 0;
            }
            this->pBall = PyObject_CallObject(pClass, NULL);
         
            this->pArgs = PyTuple_New(3);
            this->pAlliedRobotsTuple = PyTuple_New(3);
            this->pEnemyRobotsTuple = PyTuple_New(3);
        }else{
            fprintf(stderr, "Cannot find function \"%s\"\n", method);
            Py_XDECREF(pFunc);
            Py_DECREF(pModule);
            return 0;
        }
    }else{
        PyErr_Print();
        Py_XDECREF(pFunc);
        fprintf(stderr, "Failed to load \"%s\"\n", name);
        return 0;
    }

    for(int i = 0; i < 3; i++){
        PyTuple_SetItem(this->pAlliedRobotsTuple, i, this->pAlliedRobots[i]);
        PyTuple_SetItem(this->pEnemyRobotsTuple, i, this->pEnemyRobots[i]);
    }
    PyTuple_SetItem(this->pArgs, 0, this->pAlliedRobotsTuple);
	PyTuple_SetItem(this->pArgs, 1, this->pEnemyRobotsTuple);
	PyTuple_SetItem(this->pArgs, 2, this->pBall);

    return 1;

}

void VsPyInterpreter::setAlliedRobot(Robot robot, int pos){
	setLongAttribute(this->pAlliedRobots[pos], "id", robot.id);
	setFloatAttribute(this->pAlliedRobots[pos], "pos_x", robot.pos_x);
	setFloatAttribute(this->pAlliedRobots[pos], "pos_y", robot.pos_y);
	setFloatAttribute(this->pAlliedRobots[pos], "vel_x", robot.vel_x);
	setFloatAttribute(this->pAlliedRobots[pos], "vel_y", robot.vel_y);
	setFloatAttribute(this->pAlliedRobots[pos], "angle", robot.angle);
	PyTuple_SetItem(this->pAlliedRobotsTuple, pos, this->pAlliedRobots[pos]);
	PyTuple_SetItem(this->pArgs, 0, this->pAlliedRobotsTuple);
}

void VsPyInterpreter::setEnemyRobot(Robot robot, int pos){
	setLongAttribute(this->pEnemyRobots[pos], "id", robot.id);
	setFloatAttribute(this->pEnemyRobots[pos], "pos_x", robot.pos_x);
	setFloatAttribute(this->pEnemyRobots[pos], "pos_y", robot.pos_y);
	setFloatAttribute(this->pEnemyRobots[pos], "vel_x", robot.vel_x);
	setFloatAttribute(this->pEnemyRobots[pos], "vel_y", robot.vel_y);
	setFloatAttribute(this->pEnemyRobots[pos], "angle", robot.angle);
	PyTuple_SetItem(this->pAlliedRobotsTuple, pos, this->pEnemyRobots[pos]);
	PyTuple_SetItem(this->pArgs, 1, this->pAlliedRobotsTuple);
}

void VsPyInterpreter::setBall(Ball ball){
	setFloatAttribute(this->pBall, "pos_x", ball.pos_x);
	setFloatAttribute(this->pBall, "pos_y", ball.pos_y);
	setFloatAttribute(this->pBall, "vel_x", ball.vel_x);
	setFloatAttribute(this->pBall, "vel_y", ball.vel_y);
	PyTuple_SetItem(this->pArgs, 2, this->pBall);
}

int VsPyInterpreter::run(){
	PyObject *pReturnedValue = PyObject_CallObject(this->pFunc, this->pArgs);
    if (pReturnedValue != NULL) {
        PyObject *pAlliedRobotsTupleOutput = PyTuple_GetItem(pReturnedValue, 0);
        PyObject *pEnemiesRobotsTupleOutput = PyTuple_GetItem(pReturnedValue, 1);
        PyObject *pBallOutput = PyTuple_GetItem(pReturnedValue, 2);

        for(int i = 0;  i < 3; i++){
        	PyObject *pRobot = PyTuple_GetItem(pAlliedRobotsTupleOutput, i);
        	this->alliedRobotsOutput[i].id = getLongAttribute(pRobot, "id");
			this->alliedRobotsOutput[i].pos_x = getFloatAttribute(pRobot, "pos_x");
			this->alliedRobotsOutput[i].pos_y = getFloatAttribute(pRobot, "pos_y");
			this->alliedRobotsOutput[i].vel_x = getFloatAttribute(pRobot, "vel_x");
			this->alliedRobotsOutput[i].vel_y = getFloatAttribute(pRobot, "vel_y");
			this->alliedRobotsOutput[i].angle = getFloatAttribute(pRobot, "angle");

        }

        for(int i = 0;  i < 3; i++){
        	PyObject *pRobot = PyTuple_GetItem(pEnemiesRobotsTupleOutput, i);
        	this->EnemyRobotsOutput[i].id = getLongAttribute(pRobot, "id");
			this->EnemyRobotsOutput[i].pos_x = getFloatAttribute(pRobot, "pos_x");
			this->EnemyRobotsOutput[i].pos_y = getFloatAttribute(pRobot, "pos_y");
			this->EnemyRobotsOutput[i].vel_x = getFloatAttribute(pRobot, "vel_x");
			this->EnemyRobotsOutput[i].vel_y = getFloatAttribute(pRobot, "vel_y");
			this->EnemyRobotsOutput[i].angle = getFloatAttribute(pRobot, "angle");
        }

		this->ballOutput.pos_x = getFloatAttribute(pBallOutput, "pos_x");
		this->ballOutput.pos_y = getFloatAttribute(pBallOutput, "pos_y");
		this->ballOutput.vel_x = getFloatAttribute(pBallOutput, "vel_x");
		this->ballOutput.vel_y = getFloatAttribute(pBallOutput, "vel_y");

    }else{
        PyErr_Print();
        fprintf(stderr,"Call failed\n");
        return 0;
    }  

    Py_DECREF(pReturnedValue);
    return 1;
}