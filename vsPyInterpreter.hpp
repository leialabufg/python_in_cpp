#include <Python.h>
#include <string>
#include "robot_ball.hpp"

class VsPyInterpreter{

	private:
		std::string script_name;
		std::string method_name;
		
		PyObject *pModule;
		PyObject *pFunc;
    	PyObject *pArgs;
    	PyObject *pAlliedRobotsTuple;
    	PyObject *pEnemyRobotsTuple;

    	PyObject *pAlliedRobots[3];
        PyObject *pEnemyRobots[3];
        PyObject *pBall;

        Robot alliedRobots[3];
        Robot EnemyRobots[3];
        Ball ball;

        Robot alliedRobotsOutput[3];
        Robot EnemyRobotsOutput[3];
        Ball ballOutput;

    public:
    	VsPyInterpreter();
    	~VsPyInterpreter();
    	int config(std::string script_name, std::string method_name);
    	int run();
    	void setAlliedRobot(Robot robot, int pos);
    	void setEnemyRobot(Robot robot, int pos);
    	void setBall(Ball ball);

    	static void setFloatAttribute(PyObject* pyInstance, const char* attr_name, float val);
    	static double getFloatAttribute(PyObject* pyInstance, const char* attr_name);
    	static void setLongAttribute(PyObject* pyInstance, const char* attr_name, long val);
    	static long getLongAttribute(PyObject* pyInstance, const char* attr_name);


};